```
         __      __  _____ __
    ____/ /___  / /_/ __(_) /__  _____
   / __  / __ \/ __/ /_/ / / _ \/ ___/
 _/ /_/ / /_/ / /_/ __/ / /  __(__  )
(_)__,_/\____/\__/_/ /_/_/\___/____/
```

# My `.dotfiles`, managed by [dotdrop](https://github.com/deadc0de6/dotdrop)

## Encryption

Some sensitive dotfiles are encrypted using [`git-crypt`](https://github.com/AGWA/git-crypt).
These are specified in [`.gitattributes`](./.gitattributes).

## NeoVim

### Debugging

#### Debugging plugin options

```
nvim --cmd 'let g:debug_plugin_settings = 1'
```

## ZSH

ZSH plugin management is done through [`zplug`](https://github.com/zplug/zplug).
`zplug` was installed using the "best way".

```
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
```

Once it's installed, source the `.zshrc` and run:
```
zplug install
```
